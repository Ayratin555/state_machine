module StateMachine
  def self.included(base)
    base.extend ClassMethods
  end

  class StateTransitionsError < RuntimeError; end

  module ClassMethods

    def state_machine
      yield
    end

    def state(name, initial=false)
      @@initial_state ||= name if initial

      define_method "#{name}?" do
        instance_variable_get('@state').to_s==name.to_s
      end

      define_method :initialize do
        instance_variable_set('@state', @@initial_state)
        super()
      end
    end

    def event(name, callback_methods = nil, &block)
      define_method "#{name}" do
        state = change_state(callback_methods, &block)
        state
      end

      define_method "#{name}!" do
        state = change_state(callback_methods, &block)
        raise StateTransitionsError unless state
        state
      end
    end
  end

  attr_accessor :state

  private
    def change_state(callback_methods, &block)
      state = instance_eval(&block)
      callbacks(callback_methods, state)
      state
    end

    def transitions(states)
      res = states[:if_method] ? send(states[:if_method]) : true
      @state = states[:to] if ( states[:from].include?(@state) and res )
    end

    def callbacks(callback_methods, state)
      unless callback_methods.nil? or state.nil?
        after_commit_callback(callback_methods)
        #other callbacks will be added in future
      end
    end

    def after_commit_callback(callback_methods)
      send(callback_methods[:after_commit]) unless callback_methods[:after_commit].nil?
    end
end