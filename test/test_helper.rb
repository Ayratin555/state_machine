require_relative "../state_machine"

class Image
  include StateMachine
  attr_reader :last_method
  attr_accessor :if_flag

  state_machine do
    state :limb, initial: true
    state :resizing
    state :resized

    event :resize, after_commit: :send_for_processing do
      transitions from: [:limb], to: :resizing
    end

    event :complete_resize do
      transitions from: [:resizing], to: :resized, if_method: :set_thumb
    end
  end

  def send_for_processing
    @last_method = __method__
  end

  def set_thumb
    @last_method = __method__
    @if_flag
  end
end