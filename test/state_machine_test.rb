require 'test/unit'
require_relative "./test_helper"

class StateMachineTest < Test::Unit::TestCase

  def test_changes_states
    #checks initial state
    image = Image.new
    assert image.limb?, "Default state is not right!"

    #checks callbacks and transitions on resize event
    image.resize
    assert image.resizing?, "State didn't change to resizing"
    assert_equal :send_for_processing, image.last_method, "After commit method wasn't called"

    #checks callbacks and transitions on complete_resize event
    #if_flag - transition guard
    image.if_flag=true
    image.complete_resize
    assert image.resized?, "State didn't change to resized"
    assert_equal :set_thumb, image.last_method, "Guard method wasn't called "
  end

  def test_checks_wrong_transitions
    image = Image.new
    assert_equal nil, image.complete_resize, "State transition didn't return nil"
    assert image.limb?, "Default state changed!"

    image.resize
    image.if_flag=false
    assert_equal nil, image.complete_resize, "State transition didn't return nil"
    assert_raises StateMachine::StateTransitionsError do
      image.complete_resize!
    end
  end
end